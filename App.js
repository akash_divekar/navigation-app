import * as React from 'react';
import {useState, useEffect} from 'react';
import { Button, View, Text, TextInput, StyleSheet,FlatList, TouchableOpacity } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Ionicons from 'react-native-vector-icons/Ionicons';
import { WebView } from 'react-native-webview';
import AsyncStorage from '@react-native-async-storage/async-storage';



function WeatherScreen() {
  const [data, setData] = useState('');

  const getWeatherDetails = async() => {
    fetch('https://api.openweathermap.org/data/2.5/weather?q=bengaluru&appid=2ca0aad18afe1d85d679b037a7ca2be8')
    .then((response) => response.json())
    .then((responseJson) => {
      setData(responseJson)
    })
    .catch(function(error) {
      console.log('There has been a problem with your fetch operation: ' + error.message);
       // ADD THIS THROW error
        throw error;
      })
  }

  return (
    <View style={{ flex: 1,marginTop:20,marginLeft:10,marginRight:10 }}>
      <Text style={{textAlign:"center", fontSize:20}}>Weather Screen</Text>
      <View style={{marginTop:20, marginHorizontal:50}}>
      <Button 
        title="Get details"
        onPress= {() => getWeatherDetails()} />
        {
          data ? <View style={{ flex: 1, alignItems: 'center' }}>
                        <Text style={{marginTop:10}}>Location : {data.name}</Text>
                        <Text style={{marginTop:10}}>Temperature Min : {data.main.temp_min}</Text>
                        <Text style={{marginTop:10}}>Temperature Max : {data.main.temp_max}</Text>
                        <Text style={{marginTop:10}}>Pressure : {data.main.pressure}</Text>
                        <Text style={{marginTop:10}}>Humidity : {data.main.humidity}</Text>
                  </View>
                  :null
        }
        </View>
    </View>
  );
}

function SearchScreen() {
  const webview = React.useRef(null)
  return (
    <WebView
      ref={webview}
      style={{flex:1}}
      source={{uri: "https://google.com"}} />
  );
}

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

function HomeScreen() {
  return (
    <Tab.Navigator initialRouteName="Weather">
        <Tab.Screen name="Weather" component={WeatherScreen} 
        options={{tabBarIcon: () => (
          <Ionicons name="thunderstorm" size={25} />
        )}} />
        <Tab.Screen name="Search" component={SearchScreen} 
        options={{tabBarIcon: () => (
          <Ionicons name="search-circle" size={35} />
        )}}/>
    </Tab.Navigator>
  );
}

function HomeRoot({ navigation }) {
  return (
    <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} 
        options={{headerLeft: () => (
          <Ionicons
            style={{paddingLeft:10}}
            name='menu-outline'
            size={30}
            onPress={() => navigation.toggleDrawer()} />
          ) }} />
    </Stack.Navigator>
  );
}

function AboutScreen({navigation }) {
  return (
    <View style={{ flex: 1 }}>
      <Text style={{fontSize:20, marginTop:15, paddingLeft:30}}>Lorem Ipsum is simply dummy text of</Text>
      <Text style={{fontSize:20, marginLeft:10}}>the 
        printing and typesetting industry. Lorem Ipsum has been the industry's standard 
        dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled 
        it to make a type specimen book. </Text>
        <Text style={{fontSize:20, marginTop:5, paddingLeft:30}}>It has survived not only five centuries,  </Text>
        <Text style={{fontSize:20, marginLeft:10}}>but also the leap into electronic typesetting, remaining essentially unchanged. 
        It was popularised in the 1960s with the release of Letraset sheets containing 
        Lorem Ipsum passages, and more recently with desktop publishing software like 
        Aldus PageMaker including versions of Lorem Ipsum.</Text>
    </View>
  );
}

function ProfileScreen({ navigation }) {

  const [username, setUsername] = useState(null);

  useEffect(() => {
    navigation.addListener('focus', () => {
      getData();
    });
  })

  const getData = async () => {
    try {
      const uname = await AsyncStorage.getItem('uname')
      if(uname !== null) {
        setUsername(uname);
      }
    } catch(e) {
      console.log(e);
    }
  }

  return (
    <View style={{ flex: 1 }}>
      <Text style={{marginTop:10, marginLeft:10, fontSize:18}}>Name :
      {
        username ? <Text style={{marginTop:10, fontSize:18}} > {username}</Text> : 
        <Text style={{marginTop:10, fontSize:18}} >not set</Text>
      }
      </Text>
    </View>
    
  );
}



function EditProfileScreen({navigation}) {
  const [username, setUsername] = useState("");

  useEffect(() => {
    navigation.addListener('focus', () => {
      getData();
    });
  })

  const getData = async() => {
    try {
      const uname = await AsyncStorage.getItem('uname');
      setUsername(uname);
    }
    catch(e) {
      console.log(e);
    }
  }

  const saveData = async() => {
    try {
      await AsyncStorage.setItem('uname', username);
    }
    catch(e) {
        console.log(e);
    }
  }

  return (
    <View style={{ flex: 1}}>
      <Text style={{fontSize:20, marginVertical:10,marginHorizontal:10}}>Enter user name : </Text>
      <TextInput style={{color:'black',backgroundColor:'white',fontSize:15,marginHorizontal:10,marginVertical:10}} 
      onChangeText={(username) => setUsername(username)} value={ username } />
      <View style={{width:100,marginHorizontal:140}}>
        <Button title="Save" 
        onPress={() => saveData() }/>
      </View>
    </View>
  );
}

function SettingsScreen({ navigation }) {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Settings" component={SettingsRootScreen} 
      options={{headerLeft: () => (
        <Ionicons
          style={{paddingLeft:10}}
          name='menu-outline'
          size={30}
          onPress={() => navigation.toggleDrawer()} />
        ) }}/>
      <Stack.Screen name="About" component={AboutScreen} />
      <Stack.Screen name="Profile" component={ProfileScreen} 
      options={{
        headerRight: () => (
          <View style={{marginRight:15}}>
          <Button
          
          title="Edit" 
          onPress={() => navigation.navigate("Edit Profile") } /></View>
        )
      }}/>
      <Stack.Screen name="Edit Profile" component={EditProfileScreen} />
    </Stack.Navigator>
  );
}

function SettingsRootScreen({navigation}) {

  return(
    <View>
      <FlatList
        style={{marginTop:10}}
        data={[{name:"About",id:"1"},{name:"Profile",id:"2"}]}
        renderItem={({item}) =>  (
          <View style={{justifyContent:'center',marginBottom:10}}>
            <View>
              <Button title={item.name} onPress={() => navigation.navigate(item.name)} />
            </View>
          </View>)}
          keyExtractor={(item,index)=>item.id.toString()}
      />
    </View>
  );
}

const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="Home">
        <Drawer.Screen name="Home" component={HomeRoot}/>
        <Drawer.Screen name="Settings" component={SettingsScreen} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}